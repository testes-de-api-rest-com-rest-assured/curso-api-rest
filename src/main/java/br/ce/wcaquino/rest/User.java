package br.ce.wcaquino.rest;

public class User {
	
	private String name;
	private Integer idade;
	private Double salario;
	
		
	public User(String name, Integer idade) {
		super();
		this.name = name;
		this.idade = idade;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getIdade() {
		return idade;
	}
	public void setIdade(Integer idade) {
		this.idade = idade;
	}
	public Double getSalario() {
		return salario;
	}
	public void setSalario(Double salario) {
		this.salario = salario;
	}

	
}
