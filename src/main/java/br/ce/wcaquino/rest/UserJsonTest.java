package br.ce.wcaquino.rest;

import static io.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.Arrays;

import org.hamcrest.Matchers;
import org.hamcrest.collection.HasItemInArray;
import org.junit.Assert;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.matcher.ResponseAwareMatcher;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class UserJsonTest {
	
	@Test
	
	public void verificarPrimeiroNivel() {
		
		given()
		.when()
			.get("http://restapi.wcaquino.me/users/1")
		.then()
			.statusCode(200)
			.body("id", Matchers.is(1))
			.body("name", Matchers.containsString("Silva")) // cont�m
			.body("age", Matchers.greaterThan(18));			// maior que 18
			
	}
	
	@Test
	
	public void verificarPrimeiroNivelOutros() {
		
		Response response = RestAssured.request(Method.GET, "http://restapi.wcaquino.me/users/1");
		
		//path
		
		Assert.assertEquals(new Integer(1), response.path("id")); 
				
		// jsonpath
		
		JsonPath jpath = new JsonPath(response.asString());
		Assert.assertEquals(1, jpath.getInt("id"));
		
		//from
		int id = JsonPath.from(response.asString()).getInt("id");
		Assert.assertEquals(1, id);
	}
	
	@Test
	
	public void verificarSegundoNivel() {
		
		given()
		.when()
			.get("http://restapi.wcaquino.me/users/2")
		.then()
			.statusCode(200)
			.body("name", Matchers.containsString("Joaquina")) // cont�m
			.body("endereco.rua", Matchers.is("Rua dos bobos"));
		
	}
	
	@Test
	
	public void verificarTerceiroNivel() {
		
		given()
		.when()
			.get("http://restapi.wcaquino.me/users/3")
		.then()
			.statusCode(200)
			.body("name", Matchers.containsString("Ana")) 
			.body("filhos", Matchers.hasSize(2)) 			//tamanho da lista
			.body("filhos[0].name", Matchers.is("Zezinho")) //[] lista dentro de array - indicar posicao
			.body("filhos[1].name", Matchers.is("Luizinho"))
			.body("filhos.name", Matchers.hasItem("Zezinho"));
		
	}
	
	@Test
	
	public void verificarMsgErroUserQuatro() {
		
		given()
		.when()
			.get("http://restapi.wcaquino.me/users/4")
		.then()
			.statusCode(404)
			.body("error", Matchers.is("Usuário inexistente"));
			
		
	}

	@Test
	
	public void verificarListaRaiz() {
		
		given()
		.when()
			.get("http://restapi.wcaquino.me/users")
		.then()
			.statusCode(200)
			.body("$", Matchers.hasSize(3)) // lista na raiz
			.body("name", Matchers.hasItems("João da Silva", "Maria Joaquina", "Ana Júlia"))
			.body("age[1]", Matchers.is(25))
			.body("filhos.name", Matchers.hasItem(Arrays.asList("Zezinho", "Luizinho")))
			;
		
	}
	
	@Test
	
	public void verificacaoAvancada() {
		
		given()
		.when()
			.get("http://restapi.wcaquino.me/users")
		.then()
			.statusCode(200)
			.body("age.findAll{it <= 25}.size()", Matchers.is(2)) //qtos users com mais de 25 anos
			.body("salary.findAll{it != null}.sum()", Matchers.is(Matchers.closeTo(3734.5678f, 0.001))) //closeto para margem de erro de casas decimais
		
		;
	}
	
	@Test
	
	public void jsonPathcomJava() {
		ArrayList<String> names = 
		given()
		.when()
			.get("http://restapi.wcaquino.me/users")
		.then()
			.statusCode(200)
			.extract().path("name.findAll{it.startsWith('Maria')}")
		
		;
		Assert.assertEquals(1, names.size());
	}
	
}
