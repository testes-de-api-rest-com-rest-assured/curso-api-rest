package br.ce.wcaquino.rest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

import java.util.HashMap;
import java.util.Map;

import org.hamcrest.Matchers;
import org.junit.Test;

import io.restassured.http.ContentType;
import sun.security.krb5.internal.util.KerberosString;


public class VerbosTest {
	
	@Test
	
	public void deveSalvarUser() {
		
		given()
			.log().all()
			.contentType("application/json")
			.body("{\"name\": \"Jose\", \"age\": 50}")
		.when()
			.post("http://restapi.wcaquino.me/users")
		.then()
			.log().all()
			.statusCode(201)
			.body("id", is(notNullValue()))
			.body("name", is("Jose"))
			.body("age", Matchers.is(50))
			;
	}
	
	@Test
	
	public void naoSalvarUserSemNome() {
		
		given()
		.log().all()
		.contentType("application/json")
		.body("{\"age\": 50}")
	.when()
		.post("http://restapi.wcaquino.me/users")
	.then()
		.log().all()
		.statusCode(400)
		.body("id", is(nullValue()))
		.body("error", Matchers.is("Name é um atributo obrigatório"))
		
		; 
		
	}
	
		@Test
	
	public void deveSalvarUserXML() {
		
		given()
			.log().all()
			.contentType(ContentType.XML)   //nao usa json em xml // no ContentType. pode escolher o tipo / outra op��o
			.body("<user><name>Jose</name><age>50</age></user>")
		.when()
			.post("http://restapi.wcaquino.me/usersXML")
		.then()
			.log().all()
			.statusCode(201)
			.body("user.@id", is(notNullValue()))
			.body("user.name", is("Jose"))
			.body("user.age", Matchers.is("50"))
			;
	}
		
		@Test
		
		public void alterarUser() {
			
			given()
				.log().all()
				.contentType("application/json")
				.body("{\"name\": \"Usuario alterado\", \"age\": 80}")
			.when()
				.put("http://restapi.wcaquino.me/users/1")
			.then()
				.log().all()
				.statusCode(200)
				.body("id", is(1))
				.body("name", is("Usuario alterado"))
				.body("age", Matchers.is(80))
				;
		}
		
		
		@Test
		
		public void deletarUser() {
			
			given()
				.log().all()
			.when()
				.delete("http://restapi.wcaquino.me/users/1")
			.then()
				.log().all()
				.statusCode(204)
			;
		
		}
		
		@Test
		
		public void naoDeletarUserInexistente() {
			
			given()
				.log().all()
			.when()
				.delete("http://restapi.wcaquino.me/users/1000")
			.then()
				.log().all()
				.statusCode(400)
				.body("error", Matchers.is("Registro inexistente"))
			;
		
		}
		
		
		@Test
		
		public void deveSalvarUserUsandoMap() {
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("name", "Usuario via map");
			params.put("age", 25);
			
			given()
				.log().all()
				.contentType("application/json")
				.body(params)
			.when()
				.post("http://restapi.wcaquino.me/users")
			.then()
				.log().all()
				.statusCode(201)
				.body("id", is(notNullValue()))
				.body("name", is("Usuario via map"))
				.body("age", Matchers.is(25))
				;
		}

		
		@Test
		
		public void deveSalvarUserUsandoObjeto() {
			
			User user = new User("Usuario via objeto", 35);
			
			given()
				.log().all()
				.contentType("application/json") 
				.body(user)
			.when()
				.post("http://restapi.wcaquino.me/users")
			.then()
				.log().all()
				.statusCode(201)
				.body("id", is(notNullValue()))
				.body("name", is("Usuario via objeto"))
				.body("idade", Matchers.is(35))
				;
		}


}

