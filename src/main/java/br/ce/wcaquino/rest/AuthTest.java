package br.ce.wcaquino.rest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

import java.util.HashMap;
import java.util.Map;

import org.hamcrest.Matchers;
import org.junit.Test;

import io.restassured.http.ContentType;

public class AuthTest {
	
	@Test
	
	public void acessarSWAPI() {
		
		given()
			.log().all()
		.when()
			.get("https://swapi.dev/api/people/1/")
		.then()
			.log().all()
			.statusCode(200)
			.body("name", is("Luke Skywalker"))
		;
		
	}
		
	@Test
	
	public void naoAcessarSemSenha() {
		
		given()
			.log().all()
		.when()
			.get("https://restapi.wcaquino.me/basicauth")
		.then()
			.log().all()
			.statusCode(401)
			
		;
		
	}
	
	@Test
	
	public void acessarAutenticacao() {
		
		given()
			.log().all()
		.when()
			.get("https://admin:senha@restapi.wcaquino.me/basicauth") //usuario e senha admin:senha	
		.then()
			.log().all()
			.statusCode(200)
			.body("status", Matchers.is("logado"))
		;
		
	}

	@Test
	
	public void acessarAutenticacao2() {
		
		given()
			.log().all()
			.auth().basic("admin", "senha")  //usuario e senha admin:senha
		.when()
			.get("https://restapi.wcaquino.me/basicauth")	
		.then()
			.log().all()
			.statusCode(200)
			.body("status", Matchers.is("logado"))
		;
		
	}

	@Test
	
	public void acessarComTokenJWT	() {
		
		Map<String, String> login = new HashMap<String, String>();
		login.put("email", "janainaslima@gmail.com");
		login.put("senha", "123456");
		
		given()
			.log().all()
			.body(login)
			.contentType(ContentType.JSON)
		.when()
			.get("https://seubarriga.wcaquino.me/login")	
		.then()
			.log().all()
			.statusCode(200)
		;
		
	}

	@Test
	
	public void acessoAppWeb() {
		
		// login
		
	given()
		.log().all()
		.formParam("email", "janainaslima@gmail.com")
		.formParam("senha", "123456")
		.contentType(ContentType.URLENC.withCharset("UTF-8"))
	.when()
		.post("https://seubarriga.wcaquino.me/logar")	
	.then()
		.log().all()
		.statusCode(200)
	;
		
		// obter conta
		
	}
	
}
