package br.ce.wcaquino.rest;

import static io.restassured.RestAssured.given;

import org.hamcrest.Matchers;
import org.junit.BeforeClass;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class UserXMLTest {
	
	public static RequestSpecification reqSpec;
	public static ResponseSpecification resSpec;
	
	@BeforeClass
	//referente a aula sobre atributo estatico
	
	public static void setup() {
		
		RestAssured.baseURI = "http://restapi.wcaquino.me"; //conecta por aqui e nao no get// se fosse https a porta seria 443
		//RestAssured.port = 80;
		//RestAssured.basePath = "/v2";
		
		RequestSpecBuilder reqBuilder = new RequestSpecBuilder();
		reqBuilder.log(LogDetail.ALL);
		reqSpec = reqBuilder.build();
		
		ResponseSpecBuilder resBuilder = new ResponseSpecBuilder();
		resBuilder.expectStatusCode(200);
		resSpec = resBuilder.build();
		
	}
	
	@Test
	
	public void workToXML() {
		
		given()
			.spec(reqSpec)
		.when()
			.get("/usersXML/3")
		.then()
			//.statusCode(200)
			.spec(resSpec)
			.body("user.name", Matchers.is("Ana Julia"))
			.body("user.@id", Matchers.is("3")) //@ usado para referenciar um atributo - valores ficam entre "" no XMl pq sao considerados strings
			.body("user.filhos.name.size()", Matchers.is(2))
			.body("user.filhos.name", Matchers.hasItem("Luizinho"));
		}
	
	@Test
	
	public void usarNoRaiz() {
		// no raiz usado para evitar repeticao para chamar user
		
		given()
		.when()
			.get("usersXML/3")
		.then()
			.statusCode(200)
			.rootPath("user") //mesmo codigo anterior mas este comando retira o user raiz dos demais body
			.body("name", Matchers.is("Ana Julia"))
			.body("@id", Matchers.is("3")) //@ usado para referenciar um atributo - valores ficam entre "" no XMl pq sao considerados strings
			.rootPath("user.filhos") //pode usar mais de um e o comando vale para as linhas abaixo dele
			.body("name.size()", Matchers.is(2))
			.body("name", Matchers.hasItem("Luizinho"));
			;

	}
	
	@Test
		public void pesquisaXMLJava() {
		
		Object path = given()
		.when()
			.get("usersXML")
		.then()
			.extract().path("users.user.name.findAll{it.toString().startsWith('Maria')}")
		;
		System.out.println(path.toString());
	}
	
	@Test
	
	public void aulaAtributoEstatico() {
		//criar classe public static void no inicio do projeto
		
		RestAssured.baseURI = "http://restapi.wcaquino.me"; //conecta por aqui e nao no get// se fosse https a porta seria 443
		RestAssured.port = 80;
		RestAssured.basePath = "/v2";
		
		given()
			.log().all()
		.when()
			.get("/users") // como ja logou acima, aqui fica declarado apenas os recursos.
		.then()
			.statusCode(200);
	
	}
}