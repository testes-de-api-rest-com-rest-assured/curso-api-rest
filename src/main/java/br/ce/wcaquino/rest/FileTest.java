package br.ce.wcaquino.rest;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.hamcrest.Matchers;
import org.junit.Test;

public class FileTest {
	
	@Test
	
	public void envioArquivo() {
		
		given()
			.log().all()
		.when()
			.post("http://restapi.wcaquino.me/upload")
		.then()
			.log().all()
			.statusCode(404)
			.body("error", Matchers.is("Arquivo não enviado"))
		;
		
	}
	
	@Test
	
	public void envioArquivoTest() {
		
		given()
			.log().all()
			.multiPart("arquivo", new File("src/main/resources/teste.txt"))//se salvar no projeto coloca assim mas pode ser endere�o de diret�rio
		.when()
			.post("http://restapi.wcaquino.me/upload")
		.then()
			.log().all()
			.statusCode(200)
			.body("name", Matchers.is("teste.txt"))
				
			
		;
		
	}

	@Test
	
	public void downloadArquivoTest() throws IOException {
		
		byte[] image = given()
			.log().all()
		.when()
			.get("http://restapi.wcaquino.me/download")
		.then()
//			.log().all()
			.statusCode(200)
			.extract().asByteArray()				
			
		;
		File imagem = new File("src/main/resources/file.jpg");
		OutputStream out = new FileOutputStream(imagem);
		out.write(image);
		out.close();
		
		System.out.println(imagem.length());
	}

	
	
}
