package br.ce.wcaquino.rest;

import static io.restassured.RestAssured.given;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;

public class OlaMundoTest {
	
	//@Test
	public void testOlaMundo() {
		
		Response response = RestAssured.request(Method.GET, "http://restapi.wcaquino.me/ola");
		Assert.assertTrue(response.getBody().asString().equals("Ola Mundo!"));
		Assert.assertEquals(200, response.statusCode());
		//Assert.assertTrue(response.statusCode() == 200);
		
		//System.out.println(response.getBody().asString().equals("Ola Mundo!"));
		//System.out.println(response.statusCode() == 200);
		
		
		ValidatableResponse validacao = response.then();
		validacao.statusCode(200);
		
	}

	@Test
	
	public void outrosRestAssured() {
		//metodo1
		//Response response = RestAssured.request(Method.GET, "http://restapi.wcaquino.me/ola");
		//ValidatableResponse validacao = response.then();
		//validacao.statusCode(200);
		
		// metodo 2 - colocou toda programa��o em 1 linha
		
		//metodo 3
		//get("http://restapi.wcaquino.me/ola").then().statusCode(200);
		
		
		//metodo 4 - Given pre condicao - When a a��o - Then assetivas
		
		given().when().get("http://restapi.wcaquino.me/ola").then().statusCode(200);
	}
	
	@Test
	
	public void matchersHamcrest() {
		
		Assert.assertThat("Maria", Matchers.is("Maria"));
		//com source add import
		Assert.assertThat(123, Matchers.isA(Integer.class));
	}
	
	@ Test
	
	public void validarBody() {
		
		given()
		.when()
			.get("http://restapi.wcaquino.me/ola")
		.then()
			.statusCode(200)
			.body(Matchers.is("Ola Mundo!"))
			.body(Matchers.containsString("Mundo"));
			
			
			
		
	}
}
